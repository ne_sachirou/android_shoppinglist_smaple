package jp.ac.hal.IH14B_03.Kaimono;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoListSetting;


public class SettingActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TextView text = (TextView) findViewById(R.id.editText);
        text.setText(KaimonoListSetting.getInstance(getApplicationContext()).getListName());
    }

    @Override
    protected void onPause() {
        super.onPause();
        TextView text = (TextView) findViewById(R.id.editText);
        String listName = text.getText().toString();
        KaimonoListSetting.getInstance(getApplicationContext()).saveListName(listName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_back_to_main) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickBtnBackToMain(View v) {
        finish();
    }
}
