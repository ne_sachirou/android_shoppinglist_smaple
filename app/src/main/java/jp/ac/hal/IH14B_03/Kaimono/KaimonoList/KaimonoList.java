package jp.ac.hal.IH14B_03.Kaimono.KaimonoList;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by nesachirou on 15/01/14.
 */
public class KaimonoList {
    static public KaimonoList fromJson(JSONObject json) {
        KaimonoList list = new KaimonoList();
        list.setName(json.optString("listname", ""));
        JSONArray items0 = json.optJSONArray("items0");
        for (int i = 0; i < items0.length(); ++i) {
            KaimonoItem item = KaimonoItem.fromJson(items0.optJSONObject(i));
            list.addYetPurchasedItem(item);
        }
        JSONArray items1 = json.optJSONArray("items1");
        for (int i = 0; i < items1.length(); ++i) {
            KaimonoItem item = KaimonoItem.fromJson(items1.optJSONObject(i));
            list.addPurchasedItem(item);
        }
        return list;
    }

    @Getter
    @Setter
    private String name = "";
    @Getter
    private List<KaimonoItem> yetPurchasedItems = new ArrayList<>();
    @Getter
    private List<KaimonoItem> purchasedItems = new ArrayList<>();

    public void addYetPurchasedItem(KaimonoItem item) {
        yetPurchasedItems.add(item);
    }

    public void addPurchasedItem(KaimonoItem item) {
        purchasedItems.add(item);
    }

    public KaimonoItem purchase(int location) {
        if (location >= yetPurchasedItems.size()) {
            Log.i("IndexOutOfBounds", location + ">= yet " + yetPurchasedItems.size());
            return null;
        }
        KaimonoItem item = yetPurchasedItems.remove(location);
        purchasedItems.add(item);
        return item;
    }

    public KaimonoItem restorePurchase(int location) {
        if (location >= purchasedItems.size()) {
            Log.i("IndexOutOfBounds", location + ">= pur " + purchasedItems.size());
            return null;
        }
        KaimonoItem item = purchasedItems.remove(location);
        yetPurchasedItems.add(item);
        return item;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        JSONArray items0 = new JSONArray();
        for (KaimonoItem item : yetPurchasedItems) {
            items0.put(item.toJson());
        }
        JSONArray items1 = new JSONArray();
        for (KaimonoItem item : purchasedItems) {
            items1.put(item.toJson());
        }
        try {
            json.put("listname", name);
        } catch (JSONException ex) {
        }
        try {
            json.put("items0", items0);
        } catch (JSONException ex) {
        }
        try {
            json.put("items1", items1);
        } catch (JSONException ex) {
        }
        return json;
    }
}
