package jp.ac.hal.IH14B_03.Kaimono;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoList;
import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoListCache;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_IS_PURCHASED = "isPurchased";
    private static final String ARG_LOCATION = "location";
    private static final String ARG_ITEM_NAME = "itemName";

    // TODO: Rename and change types of parameters
    private boolean isPurchased;
    private int location;
    private String itemName;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemName Parameter 1.
     * @return A new instance of fragment ItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ItemFragment newInstance(boolean isPurchased, int location, String itemName) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_IS_PURCHASED, isPurchased);
        args.putInt(ARG_LOCATION, location);
        args.putString(ARG_ITEM_NAME, itemName);
        fragment.setArguments(args);
        return fragment;
    }

    public ItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isPurchased = getArguments().getBoolean(ARG_IS_PURCHASED);
            location = getArguments().getInt(ARG_LOCATION);
            itemName = getArguments().getString(ARG_ITEM_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_item, container, false);
        TextView text = (TextView) v.findViewById(R.id.item_name);
        text.setText(itemName);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItemName(v);
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public void onClickItemName(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(itemName);
        builder.setSingleChoiceItems(new String[]{"未購入", "購入済", "削除"}, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                KaimonoList list = KaimonoListCache.getInstance().getCache();
                switch (which) {
                    case 0:
                        if (!isPurchased) {
                            break;
                        }
                        list.restorePurchase(location);
                        break;
                    case 1:
                        if (isPurchased) {
                            break;
                        }
                        list.purchase(location);
                        break;
                    case 2:
                        if (isPurchased) {
                            list.getPurchasedItems().remove(location);
                        } else {
                            list.getYetPurchasedItems().remove(location);
                        }
                        break;
                }
                MainActivity activity = (MainActivity) getActivity();
                activity.redrawList(list);
                activity.saveList();
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
