package jp.ac.hal.IH14B_03.Kaimono;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import fj.data.Either;
import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoItem;
import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoList;
import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoListApi;
import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoListCache;
import jp.ac.hal.IH14B_03.Kaimono.KaimonoList.KaimonoListSetting;


public class MainActivity extends ActionBarActivity
        implements YetPurchasedFragment.OnFragmentInteractionListener,
        PurchasedFragment.OnFragmentInteractionListener,
        ItemFragment.OnFragmentInteractionListener {

    private YetPurchasedFragment yetPurchasedFragment;
    private PurchasedFragment purchasedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ActionBar ab = getSupportActionBar();
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.Tab tabYetPurchased = ab.newTab();
        tabYetPurchased.setText(R.string.yet_purchased);
        yetPurchasedFragment = new YetPurchasedFragment();
        tabYetPurchased.setTabListener(new MainTabListener(yetPurchasedFragment));
        ActionBar.Tab tabPurchased = ab.newTab();
        tabPurchased.setText(R.string.purchased);
        purchasedFragment = new PurchasedFragment();
        tabPurchased.setTabListener(new MainTabListener(purchasedFragment));
        ab.addTab(tabYetPurchased);
        ab.addTab(tabPurchased);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!KaimonoListSetting.getInstance(getApplicationContext()).hasSetting()) {
            goToSetting();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                goToSetting();
                return true;
            case R.id.action_manually_load:
                loadList();
                return true;
            case R.id.action_manually_save:
                saveList();
                return true;
            case R.id.action_add_item:
                createNewItem();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    class MainTabListener implements ActionBar.TabListener {
        private Fragment fragment;

        public MainTabListener(Fragment fragment) {
            this.fragment = fragment;
        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            fragmentTransaction.remove(fragment);
        }

        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            fragmentTransaction.add(R.id.mainTabContent, fragment);
        }
    }

    private void goToSetting() {
        startActivity(new Intent(this, SettingActivity.class));
    }

    private void createNewItem() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("項目追加");
        final EditText editText = new EditText(this);
        editText.setSingleLine();
        builder.setView(editText);
        builder.setPositiveButton("追加", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String itemName = editText.getText().toString();
                KaimonoList list = KaimonoListCache.getInstance().getCache();
                if (null == list) {
                    list = new KaimonoList();
                    list.setName(KaimonoListSetting.getInstance(getApplicationContext()).getListName());
                    KaimonoListCache.getInstance().setCache(list);
                }
                KaimonoItem item = new KaimonoItem();
                item.setName(itemName);
                list.addYetPurchasedItem(item);
                redrawList(list);
                saveList();
            }
        });
        builder.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setCancelable(true);
        builder.create().show();
    }

    public void loadList() {
        String listName = KaimonoListSetting.getInstance(getApplicationContext()).getListName();
        final ProgressDialog dialog = ProgressDialog.show(this, "Loading...", "買い物リストを読込中です");
        try {
            new KaimonoListApi().loadToCache(listName, new AsyncTask<Either<Exception, KaimonoList>, Integer, Integer>() {
                @Override
                protected Integer doInBackground(Either<Exception, KaimonoList>... params) {
                    dialog.dismiss();
                    Either<Exception, KaimonoList> result = params[0];
                    if (result.isLeft()) {
                        Exception ex = result.left().value();
                        Log.e("loadList", ex.getMessage(), ex);
                        Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG);
                        return null;
                    }
                    redrawList(KaimonoListCache.getInstance().getCache());
                    return null;
                }
            });
        } catch (IOException ex) {
            Log.e("loadList", ex.getMessage(), ex);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG);
        }
    }

    public void saveList() {
        try {
            new KaimonoListApi().saveFromCache(new AsyncTask<Either<Exception, KaimonoList>, Integer, Integer>() {
                @Override
                protected Integer doInBackground(Either<Exception, KaimonoList>... params) {
                    Either<Exception, KaimonoList> result = params[0];
                    if (result.isLeft()) {
                        Exception ex = result.left().value();
                        Log.e("saveList", ex.getMessage(), ex);
                        Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG);
                        return null;
                    }
                    return null;
                }
            });
        } catch (IOException ex) {
            Log.e("loadList", ex.getMessage(), ex);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG);
        }
    }

    public void redrawList(KaimonoList kaimonoList) {
        if (null == kaimonoList) {
            return;
        }
        yetPurchasedFragment.redrawList(kaimonoList);
        purchasedFragment.redrawList(kaimonoList);
    }
}
