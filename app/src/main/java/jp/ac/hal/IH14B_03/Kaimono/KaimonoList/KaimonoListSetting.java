package jp.ac.hal.IH14B_03.Kaimono.KaimonoList;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by nesachirou on 15/01/14.
 */
public class KaimonoListSetting {
    private static KaimonoListSetting instance;

    public static synchronized KaimonoListSetting getInstance(Context context) {
        if (null == instance) {
            instance = new KaimonoListSetting(context);
        }
        return instance;
    }

    private SharedPreferences pref;

    private KaimonoListSetting(Context context) {
        pref = context.getSharedPreferences("kaimono", context.MODE_PRIVATE);
    }

    public boolean hasSetting() {
        return pref.contains("listName");
    }

    public String getListName() {
        return pref.getString("listName", "");
    }

    public void clear() {
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("listName");
        editor.commit();
    }

    public void saveListName(String listName) {
        listName = listName.trim();
        if (listName.isEmpty()) {
            clear();
            return;
        }
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("listName", listName);
        editor.commit();
    }
}
