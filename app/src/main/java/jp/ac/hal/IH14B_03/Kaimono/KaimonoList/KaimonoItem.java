package jp.ac.hal.IH14B_03.Kaimono.KaimonoList;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;

/**
 * Created by nesachirou on 15/01/14.
 */
@Data
public class KaimonoItem {
    static public final int YET_PURCHASED = 0;
    static public final int PURCHED = 1;

    static public KaimonoItem fromJson(JSONObject json) {
        KaimonoItem item = new KaimonoItem();
        item.setName(json.optString("name", ""));
        switch (json.optString("state")) {
            case "未購入":
                item.setState(YET_PURCHASED);
                break;
            case "購入済":
                item.setState(PURCHED);
                break;
            default:
                item.setState(YET_PURCHASED);
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            item.setCreated(format.parse(json.optString("created")));
        } catch (ParseException ex) {
            item.setCreated(new Date());
        }
        try {
            item.setUpdated(format.parse(json.optString("updated")));
        } catch (ParseException ex) {
            item.setUpdated(new Date());
        }
        return item;
    }

    private String name = "";
    private int state = YET_PURCHASED;
    private Date created = new Date();
    private Date updated = new Date();

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
        } catch (JSONException ex) {
        }
        try {
            json.put("state", PURCHED == state ? "購入済" : "未購入");
        } catch (JSONException ex) {
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            json.put("created", format.format(created));
        } catch (JSONException ex) {
        }
        try {
            json.put("updated", format.format(updated));
        } catch (JSONException ex) {
        }
        return json;
    }
}
