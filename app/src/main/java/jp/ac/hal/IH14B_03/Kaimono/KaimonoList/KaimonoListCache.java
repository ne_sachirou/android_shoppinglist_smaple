package jp.ac.hal.IH14B_03.Kaimono.KaimonoList;

import lombok.Data;

/**
 * Created by nesachirou on 15/01/15.
 */
@Data
public class KaimonoListCache {
    static private final KaimonoListCache instance = new KaimonoListCache();

    static public KaimonoListCache getInstance() {
        return instance;
    }

    private KaimonoList cache;

    private KaimonoListCache() {
    }
}
