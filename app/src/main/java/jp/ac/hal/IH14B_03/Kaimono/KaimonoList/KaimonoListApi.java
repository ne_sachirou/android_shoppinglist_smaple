package jp.ac.hal.IH14B_03.Kaimono.KaimonoList;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fj.data.Either;

/**
 * Created by nesachirou on 15/01/14.
 */
public class KaimonoListApi {
    private final String apiUrl = "http://apibox.sakura.ne.jp/st41/kaimono/api.php";

    public void load(String listName, final AsyncTask<Either<Exception, KaimonoList>, Integer, Integer> task)
            throws IOException {
        JSONObject json = new JSONObject();
        try {
            json.put("action", "getlist");
            json.put("listname", listName);
        } catch (JSONException ex) {
        }
        (new AsyncTask<JSONObject, Integer, Integer>() {
            @Override
            protected Integer doInBackground(JSONObject... params) {
                JSONObject res;
                try {
                    res = postJsonApi(params[0]);
                } catch (IOException ex) {
                    Either<Exception, KaimonoList> arg = Either.left((Exception) ex);
                    task.execute(arg);
                    return null;
                }
                Either<Exception, KaimonoList> arg = Either.right(KaimonoList.fromJson(res));
                task.execute(arg);
                return null;
            }
        }).execute(json);
    }

    public void save(final KaimonoList list, final AsyncTask<Either<Exception, KaimonoList>, Integer, Integer> task)
            throws IOException {
        JSONObject json = list.toJson();
        try {
            json.put("action", "putlist");
        } catch (JSONException ex) {
        }
        (new AsyncTask<JSONObject, Integer, Integer>() {
            @Override
            protected Integer doInBackground(JSONObject... params) {
                JSONObject res;
                try {
                    res = postJsonApi(params[0]);
                } catch (IOException ex) {
                    Either<Exception, KaimonoList> arg = Either.left((Exception) ex);
                    task.execute(arg);
                    return null;
                }
                Either<Exception, KaimonoList> arg = Either.right(list);
                task.execute(arg);
                return null;
            }
        }).execute(json);
    }

    public void loadToCache(String listName, final AsyncTask<Either<Exception, KaimonoList>, Integer, Integer> task)
            throws IOException {
        load(listName, new AsyncTask<Either<Exception, KaimonoList>, Integer, Integer>() {
            @Override
            protected Integer doInBackground(Either<Exception, KaimonoList>... params) {
                if (params[0].isRight()) {
                    KaimonoListCache.getInstance().setCache(params[0].right().value());
                }
                task.execute(params[0]);
                return null;
            }
        });
    }

    public void saveFromCache(final AsyncTask<Either<Exception, KaimonoList>, Integer, Integer> task)
            throws IOException {
        KaimonoList list = KaimonoListCache.getInstance().getCache();
        if (null == list) {
            Either<Exception, KaimonoList> arg = Either.right(null);
            task.execute(arg);
            return;
        }
        save(list, task);
    }

    private JSONObject postJsonApi(JSONObject json) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost req = new HttpPost(apiUrl);
        req.addHeader("Content-type", "application/json");
        try {
            try {
                Log.v("KaimonoListApi", json.toString(2));
            } catch (JSONException ex) {
            }
            req.setEntity(new StringEntity(json.toString(), "UTF-8"));
        } catch (UnsupportedEncodingException ex) {
        }
        return client.execute(req, new ResponseHandler<JSONObject>() {
            @Override
            public JSONObject handleResponse(HttpResponse res) throws ClientProtocolException, IOException {
                if (200 != res.getStatusLine().getStatusCode()) {
                    Log.v("KaimonoListApi", res.toString());
                    throw new IOException(res.toString());
                }
                String body = EntityUtils.toString(res.getEntity(), "UTF-8");
                Log.v("KaimonoListApi", body);
                try {
                    return new JSONObject(body);
                } catch (JSONException ex) {
                    throw new IOException(ex);
                }
            }
        });
    }
}
