#! /usr/bin/ruby
# coding=UTF-8

require 'uri'
require 'net/http'
require 'json'

ENDPOINT = 'http://apibox.sakura.ne.jp/st41/kaimono/api.php'

def post_json body
  uri = URI.parse ENDPOINT
  req = Net::HTTP::Post.new uri.request_uri,
    'Content-Type' => 'application/json'
  req.body = body.to_json
  res = Net::HTTP.new(uri.host, uri.port).start{|http| http.request req }
  res.is_a?(Net::HTTPOK) ? JSON.parse(res.body) : res
end

def getlist list_name
  post_json({ action: 'getlist', listname: list_name })
end

def putlist list_name, items0_names, items1_names
  items = ->(names, state)do
    names.map do|name|
      {
        name:    name,
        state:   state,
        created: Time.now.strftime('%Y-%m-%d %H:%M:%S'),
        updated: Time.now.strftime('%Y-%m-%d %H:%M:%S'),
      }
    end
  end
  post_json({
    action:   'putlist',
    listname: list_name,
    items0:   items.call(items0_names, '未購入'),
    items1:   items.call(items1_names, '購入済'),
  })
end


require 'pp'
list_name = 'mOmonga'

pp getlist list_name

pp putlist list_name, [
  '島風くん♬♪♡',
  'sinakaze kun',
], [
  '.｡oO(さっちゃんですよヾ(〃l _ l)ﾉﾞ☆)',
  'ももんが',
]

pp getlist list_name
